#include <iostream>
#include <fstream>
#include <istream>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "library/enumerator.hpp"
#include "library/seqinfileenumerator.hpp"
#include "library/stringstreamenumerator.hpp"
#include "library/summation.hpp"

#include "library/maxsearch.hpp"



using namespace std;

struct Fish{
    string name;
    int size;
    int cnt;
    
    friend istream& operator>>(istream &is, Fish& f){
        is >> f.name >> f.size;
        return is;
    }

    Fish(){}
    Fish(string cn, int cavg, int fcnt): name(cn), size(cavg), cnt(fcnt) {}
};


struct Competitor{
    string name;
    string id;
    double avg;
    int cnt;
    int ccnt;

    bool isZero;

    
    friend istream& operator>>(istream& is, Competitor &c);

    Competitor(){}
    Competitor(string cname, string cid, double cavg, int ccnt): name (cname), id(cid), avg(cavg), cnt(ccnt) {}
    Competitor(string n, int c): name(n), cnt(c){}
};


class bigCom : public Summation<Fish, Fish>{
    Fish func(const Fish& f) const override { return Fish(f.name, f.size, 1);}
    Fish neutral() const override { return Fish("",0,0);}
    Fish add(const Fish& a, const Fish& b) const override{
         return Fish("avg", a.size + b.size, a.cnt + b.cnt);
    }
};



class cummyCom : public Summation<Competitor, ostream>{
    public:
        cummyCom(ostream *o) : Summation<Competitor, ostream>::Summation(o) { }
    protected:
        string func(const Competitor& e) const override {return e.name +" " +e.id+" "+ to_string(e.avg) + " \n";}
        bool cond(const Competitor& e) const override { return e.avg > 30; }
};


istream& operator>>(istream& is, Competitor &c){

    string line;
    getline(is, line, '\n');

    stringstream ss(line);
    ss >> c.name >> c.id;

    bigCom f;
    StringStreamEnumerator<Fish> enor(ss);
    f.addEnumerator(&enor);
    f.run();
    c.cnt = f.result().cnt;
    c.avg = (f.result().cnt == 0) ?  0 : (f.result().size / f.result().cnt);
    c.isZero = f.result().size == 0;
    
    return is;
};




// Second Question

class zeroCom : public Summation<Competitor, Competitor>{
    private:
        string _name;
    public:
        zeroCom(const string& name): _name(name) {}
    
    protected:
        Competitor func(const Competitor& e) const {return Competitor(e.name, 1);}
        Competitor neutral() const {return Competitor("", 0);}
        Competitor add(const Competitor &a, const Competitor &b) const {return Competitor(a.name, a.ccnt + b.ccnt);}

        //bool whileCond(const Competitor& current) const { return current.name == _name ;}
        bool cond(const Competitor& e) const { return e.isZero && e.name == _name; }
        //void first() {}
};

class CompEnor : public Enumerator<Competitor>{
    private:
        SeqInFileEnumerator<Competitor>* _f;
        Competitor _c;
        bool _end;
    
    public:
        CompEnor(const string &fname) {_f = new SeqInFileEnumerator<Competitor>(fname);}
        ~CompEnor() {delete _f;}
        void first() override {_f ->first(); next();}
        void next() override;
        bool end() const override { return _end;}
        Competitor current() const override {return _c;}
};


void CompEnor::next(){
    _end = _f->end();
    if(_end) return;
    _c.name = _f->current().name;
    zeroCom pr(_c.name);
    pr.addEnumerator(_f);
    pr.run();
    _c.ccnt = pr.result().ccnt;
}

class maxZero : public MaxSearch<Competitor, int>{
    int func(const Competitor& e ) const { return e.ccnt; }
};

int main(){
    string f = "inp.txt";

    cummyCom pr(&cout);
    SeqInFileEnumerator<Competitor> enor(f);
    pr.addEnumerator(&enor);
    pr.run();

    maxZero pr2;
    CompEnor enor2(f);
    pr2.addEnumerator(&enor2);

    pr2.run();

    if(pr2.found() ){
        cout << "max comptetions without fish is: " << pr2.optElem().name << endl;
    } else{
        cout << "  there is no angler with zero " << endl;
    }

    return 0;
}

