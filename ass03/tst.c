using namespace std;

struct Result{
    int sum;
    int count;

    Result(){}
    Result(int s , int c)
    {
        sum = s;
        count = c;
    }
};
struct Fish
{
    string FishName;
    int FishSize;
    friend istream& operator>>(istream& is, Fish &a);
};

class Average : public Summation< Fish , Result >
{
    Result func(const Fish& e) const override{return Result(e.FishSize , 1);  }
    Result neutral() const override{ return Result(0,0); }
    Result add( const Result& a, const Result& b) const override{return Result(a.sum + b.sum , a.count + b.count);}
};


istream& operator>>(istream& is, Fish &a)
{
    is >> a.FishName >> a.FishSize ;
    return is;
}

struct Angler{

    string name;
    string comID;
    Fish fish;
    double avg;
    int cnt = 0;

    friend istream& operator>>(istream& is, Angler &a);
};



istream& operator>>(istream& is, Angler &a)
{
    string line;
    int found;
    getline(is , line , '\n');
    stringstream ss(line);

    ss >> a.name >> a.comID ;


    Average pr;
    StringStreamEnumerator<Fish> ssenor(ss);
    pr.addEnumerator(&ssenor);
    pr.run();

    if (pr.result().count > 0)
    {
        a.avg = double(pr.result().sum) / pr.result().count;
    } else
    {
        //a.cnt++;
        a.avg = 0;

    }

    return is;
}

class assort : public Summation<Angler , ostream>
{
    public:
        assort(ostream *o) : Summation<Angler, ostream>::Summation(o) { }
    protected:
    string func(const Angler& e) const override {return e.name +" " +e.comID+" "+ to_string(e.avg) + " \n";}
    bool cond(const Angler& e) const override { return e.avg > 30; }
};

int main(){
    string input = "Tests/inp5.txt";
    assort pr(&cout);
    SeqInFileEnumerator<Angler> enor (input);
    pr.addEnumerator(&enor);
    pr.run();