#pragma once
#include <bits/types/FILE.h>
#include <iostream>
#include <fstream>

using namespace std;

struct Evaluation {
    string neptun;
    double mark;
    Evaluation (string str, double j) : neptun(str), mark(j) {}
};

class OutFile{
    public:
        enum Errors{FILE_ERROR};
        OutFile(string fname){
            x.open(fname.c_str());
            if(x.fail()) throw FILE_ERROR;
        }
        void write(const Evaluation &dx);

    private:
        ofstream x;
};
