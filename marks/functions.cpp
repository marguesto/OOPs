#include <iostream>
#include <fstream>
#include <vector>
//#include <bits/stdc++.h>

using namespace std;
 
bool cond( const vector<int> &marks, const string &pm){
    bool l = true;
    for(unsigned int i=0; l && i < marks.size(); ++i){
        l = marks[i] > 1;
    }

    int p, m; p = m = 0;
    for(unsigned int i =0; i<pm.size(); ++i){
        if(pm[i] == '+') ++p;
        if(pm[i] == '-') ++m;
    }
    return l && m<=p;
} 


double avg(const vector<int> &marks){
    double s= 0.0;
    for(unsigned int i=0; i< marks.size(); ++i){
        s+= marks[i];
    }
    return ( 0 == marks.size() ? 0 : s / marks.size());
}
