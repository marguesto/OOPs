#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "InpFile.h"

//#include <bits/stdc++.h>

using namespace std;

bool InpFile::read(Student &dx, Status &sx){
    string line;
    getline(x,line);
    if(!x.fail() && line !=""){
        sx = norm;
        istringstream in(line);
        in >> dx.neptun;
        in >> dx.pm;
        dx.marks.clear();
        int mark;
        while ( in >> mark ) {
            dx.marks.push_back(mark);
        }
    } else sx=abnorm;
    return norm ==sx;
}
