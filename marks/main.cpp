#include <iostream>
#include <fstream>
#include <vector>
#include "InpFile.h"
#include "OutFile.h"

//#include <bits/stdc++.h>

using namespace std;

bool cond(const vector<int>  &marks, const string &pm );
double avg(const vector<int> &marks);

int main(){
    try {
        InpFile x("inp");
        OutFile y("out");
        Student dx;
        Status sx;
        while(x.read(dx, sx)){
            if(cond(dx.marks,dx.pm)){
                Evaluation dy(dx.neptun, avg(dx.marks));
                y.write(dy);
            }
        } 
    } catch (InpFile::Errors err) {
        if(err==InpFile::FILE_ERROR) cout << "Error Opening the file" << endl;
    } catch (OutFile::Errors err) {
        if(err==OutFile::FILE_ERROR) cout << "Error writing the file" << endl;
    }

    return 0;
}

