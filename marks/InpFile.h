#pragma once
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

struct Student{
    string neptun;
    string pm;
    vector<int> marks;
}; 

enum Status{ norm, abnorm};

class InpFile{
    public:
        enum Errors{FILE_ERROR};

        InpFile(string fname){
            x.open(fname.c_str());
            if(x.fail()) throw FILE_ERROR;
        }
        bool read( Student &dx, Status &sx);

    private:
        ifstream x;
};
