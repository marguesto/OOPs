#include <iostream>
#include <fstream>
#include <iomanip>
#include "OutFile.h"
//#include <bits/stdc++.h>

using namespace std;

void OutFile::write(const Evaluation &dx) {
    x << dx.neptun << setw(7) << dx.mark << endl;
}
