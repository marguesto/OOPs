#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include "area.h"
#include "navigator.h"

using namespace std;



Navigator::Navigator(string s){
    _fname = s;
    ifstream f(_fname);
    if(f.fail()){ cout << "wrong file name!" << endl; exit(1);};

    f >> _n;
    _areas.resize(_n);

    for(int i=0; i<_n;++i){
        string pre; string owner; char t; int w;
        f >> pre >> owner >> t >> w;
        switch (t) {
            case 'P' : _areas[i] = new Plain(pre, owner, w,0); break;
            case 'G' : _areas[i] = new GrassLand(pre, owner, w,0); break;
            case 'L' : _areas[i] = new Lakes(pre, owner, w,0); break;
        }
    }

    f >> _hum;

    for(int i=0;i<_n;++i)
        _areas[i]->setHum(_hum);
}

void Navigator::print(){
    for(int i=0;i<_n; ++i){
        string pre = _areas[i] -> getPre();
        string owner = _areas[i]->getOwner();
        char type = _areas[i]-> getType();
        int water = _areas[i]-> getWater();
        
        cout << pre << " " << owner << " " << type << " " << water << endl;
    }
}


void Navigator::checkTypes(){
    for(int i=0; i<_n; ++i){

        string p = _areas[i]->getPre();
        string o = _areas[i]->getOwner();
        int w = _areas[i]->getWater() >= 0  ? _areas[i]->getWater() : 0 ;
        int h = _areas[i]->getHum();

        if(_areas[i]->getWater() > 50){ // lakes
            _areas[i] = new Lakes(p,o,w,h);
        } else if (_areas[i]->getWater() < 16){ // plain
            _areas[i] = new Plain(p,o,w,h);
        } else{ // grassland
            _areas[i] = new GrassLand(p,o,w,h);
        }
    }
}

// what fucking humidity means
void Navigator::nextRound(){
    for(int i=0; i<_n; ++i){
        int he = _areas[i]->getHum();
        if(he > 70){
            _areas[i]->nextRound('R');
        } else if( he < 40){
            _areas[i]->nextRound('S');
        } else if( he < 70 && he > 65 ){
            _areas[i]->nextRound('T');
        } else{
            _areas[i]->nextRound('C');
        }
    }
}

void Navigator::maxWater(){
    Area* max = _areas[0];
    for(int i=0;i<_n;++i){
        if (max->getWater() < _areas[i] -> getWater()){
            max = _areas[i];
        }
    }
    cout << max->getPre() << " " << max->getOwner() << " " << max->getType() << " " << max->getWater() << endl;
}