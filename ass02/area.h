#pragma once

#include <string>

using namespace std;

class Area{
    protected:
        string _preOwner;
        string _owner;
        unsigned int _water;
        int _hum;
        char _type;

        Area(string &pstr, string &str, char ty, int e,int h) : _preOwner(pstr), _owner(str), _water(e), _type(ty),_hum(h){}

    public:
        string getPre() {return _preOwner; };
        string getOwner(){ return _owner;};
        char getType(){return _type;};
        int getWater(){return _water;};
        int getHum() {return _hum;};

        void setType(char s){ _type =s; };
        void setHum(int h){_hum = h; };
        void changeHum(int h){_hum +=h; };

        void changeWater(int n){ _water += n;};
        virtual void nextRound(char we) = 0;
};

class Plain: public Area {
public:
    Plain(string &pstr, string &str, int e, int h): Area(pstr, str, 'P' , e, h){}
    void nextRound(char we) override ;
};

class GrassLand: public Area{
public:
    GrassLand(string &pstr, string &str, int e, int h): Area(pstr, str, 'G' , e,h){}
    void nextRound(char we) override ;
};

class Lakes: public Area{
public:
    Lakes(string &pstr, string &str, int e, int h): Area(pstr, str, 'L' , e,h){}
    void nextRound(char we) override ;
};
