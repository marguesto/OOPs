#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include "navigator.h"

//#include <bits/stdc++.h>

using namespace std;


int main(){
    Navigator nav("inp.txt");

    for(int i=0; i<10; ++i){

        nav.print();
        cout << nav.getHum() << endl;
        cout << " ---------------- " << endl;
        nav.nextRound();
        nav.checkTypes();
    }
    nav.maxWater();
    return 0;
}
