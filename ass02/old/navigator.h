#pragma once

#include <functional>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "area.h"

using namespace std;

class Navigator{
    public:
        Navigator(string s);
        void print();
        void checkTypes();
        void nextRound();
        void maxWater();
        int getHum(){return _hum;}

    private: 
        string _fname;
        vector<Area*> _areas;
        int _hum;
        int _n;
};