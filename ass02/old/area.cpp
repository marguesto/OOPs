#include <iostream>
#include <fstream>
#include <string>
#include "area.h"

using namespace std;

int Plain::nextRound(char we) {
    switch (we) {
        case('S'): changeWater(-3); break;
        case('C'): changeWater(-1); break;
        case('R'): changeWater(+20); break;
    }
    return 5;
}

int GrassLand::nextRound(char we) {
    switch (we) {
        case('S'): changeWater(-6); break;
        case('C'): changeWater(-2); break;
        case('R'): changeWater(+15); break;
    }
    return 10;
}

int Lakes::nextRound(char we){
    switch (we) {
        case('S'): changeWater(-10); break;
        case('C'): changeWater(-3); break;
        case('R'): changeWater(+20); break;
    }
    return 15;
}
