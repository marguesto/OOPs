#pragma once

#include <string>

using namespace std;

class Area{
    protected:
        string _preOwner;
        string _owner;
        int _water;
        char _type;

        Area(string &pstr, string &str, char ty, int e) : _preOwner(pstr), _owner(str), _water(e), _type(ty){}

    public:
        string getPre() {return _preOwner; };
        string getOwner(){ return _owner;};
        char getType(){return _type;};
        int getWater(){return _water;};

        void setType(char s){ _type =s; };

        void changeWater(int n){ _water += n;};
        virtual int nextRound(char we) = 0;
};

class Plain: public Area {
public:
    Plain(string &pstr, string &str, int e): Area(pstr, str, 'P' , e){}
    int nextRound(char we) override ;
};

class GrassLand: public Area{
public:
    GrassLand(string &pstr, string &str, int e): Area(pstr, str, 'G' , e){}
    int nextRound(char we) override ;
};

class Lakes: public Area{
public:
    Lakes(string &pstr, string &str, int e): Area(pstr, str, 'L' , e){}
    int nextRound(char we) override ;
};
