#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include "navigator.h"

//#include <bits/stdc++.h>

using namespace std;


int main(){

    cout << "Enter file name: ";
    string fname; cin >> fname;
    Navigator nav(fname);

    for(int i=0; i<10; ++i){

        nav.print();
        nav.nextRound();
        nav.checkTypes();
        cout << "---------------- " << endl;
    }

    nav.print();
    cout << "---------------- " << endl;
    cout << "Max: "; nav.maxWater();

    return 0;
}
